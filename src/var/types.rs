use crate::sql::schema::{gen_enum_meta_tbl, gen_list_meta_tbl, AsSqlValue};
use std::{collections::HashSet, error::Error, vec};

use super::parser::{AcqExpr as AE, VParseErr};

use chrono::{NaiveDate, NaiveDateTime, NaiveTime};
use itertools::Itertools;
use regex::Regex;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum VTypeDef {
    Int,
    String,
    DateTime { fmts: Vec<String> },
    Enum { variants: Vec<String> },
    List { item_type: Box<VTypeDef> },
}

impl VTypeDef {
    pub const T_STRING: &'static str = "str";
    pub const T_INT: &'static str = "int";
    pub const T_DATE_TIME: &'static str = "datetime";
    pub const T_ENUM: &'static str = "enum";
    pub const T_LIST: &'static str = "list";

    pub const VALID_TYPE_NAMES: &'static [&'static str] = &[
        Self::T_INT,
        Self::T_STRING,
        Self::T_DATE_TIME,
        Self::T_ENUM,
        Self::T_LIST,
    ];

    pub const VALID_LIST_ITEM_TYPE_NAMES: &'static [&'static str] =
        &[Self::T_INT, Self::T_STRING, Self::T_DATE_TIME, Self::T_ENUM];
}

#[derive(Debug)]
pub enum VTypeAnnoDetails {
    Literal(String),
    LiteralList(Vec<String>),
    NestedType(VTypeDef),
}

#[derive(Debug, PartialEq, Eq)]
pub enum VVal {
    Int {
        val: Option<i64>,
    },
    String {
        val: Option<String>,
    },
    DateTime {
        val: Option<NaiveDateTime>,
        fmt: Option<String>,
    },
    Enum {
        val: Option<String>,
    },
    List {
        val: Vec<VVal>,
    },
}

#[derive(Debug)]
pub enum AssignmentValue {
    Single(Option<String>),
    List(Vec<AssignmentValue>),
}

impl std::fmt::Display for AssignmentValue {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            AssignmentValue::Single(str) => format!("'{}'", str.as_deref().unwrap_or("NULL")),
            AssignmentValue::List(list) => {
                let str = list.iter().take(3).map(|x| return x.to_string()).join(",");
                format!(
                    "[{str}{tail}]",
                    tail = if list.len() <= 3 { "" } else { ", ..." }
                )
            }
        };

        return write!(f, "{str}");
    }
}

#[derive(Debug, Error)]
pub enum AssignmentHardError {
    #[error("{pre} Value '{val}' has incorrect shape for type {target_type}. Value needs to be of the shape {shape} for type {target_type}",  pre = log_prefix_var_assignment(var_name, *scope_id))]
    InvalidValueShape {
        val: AssignmentValue,
        shape: AssignmentShape,
        target_type: VTypeDef,
        var_name: String,
        scope_id: usize,
    },
}

#[derive(Debug, Error)]
pub enum AssignmentSoftError {
    #[error("Value is invalid enum variant. Valid variants are [{valid}]", valid = valid_variants.join(", "))]
    InvalidEnumVariant { valid_variants: Vec<String> },
}

#[derive(Debug)]
pub enum AssignmentShape {
    Single,
    List,
}

impl std::fmt::Display for AssignmentShape {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            Self::Single => "'{single-literal}'",
            Self::List => "'{list}','{of}','{literals}'",
        };

        return write!(f, "{str}");
    }
}

impl VVal {
    pub fn is_some(&self) -> bool {
        return match self {
            VVal::DateTime { val: None, .. }
            | VVal::String { val: None }
            | VVal::Int { val: None }
            | VVal::Enum { val: None } => false,
            VVal::DateTime { val: Some(_), .. }
            | VVal::String { val: Some(_) }
            | VVal::Int { val: Some(_) }
            | VVal::Enum { val: Some(_) }
            | VVal::List { .. } => true,
        };
    }

    #[allow(unused)]
    pub fn is_none(&self) -> bool {
        return !self.is_some();
    }

    pub fn sql_data_str(&self) -> Option<String> {
        return match self {
            VVal::Int { val } => val.map(|x| return x.as_sql_value()),
            VVal::String { val } | VVal::Enum { val } => {
                val.as_ref().map(|x| return x.as_sql_value())
            }
            VVal::DateTime { val, .. } => val.map(|v| return v.as_sql_value()),
            VVal::List { .. } => None,
        };
    }

    pub fn from_acq(
        acq: AE,
        tdef: &VTypeDef,
        data: &str,
        var_name: &str,
        scope_id: usize,
    ) -> Result<Self, AssignmentHardError> {
        if let VTypeDef::List { .. } = tdef {
            return match acq {
                AE::Constant { val } => Self::from_list(
                    vec![AssignmentValue::Single(Some(val))],
                    tdef,
                    var_name,
                    scope_id,
                ),
                AE::ConstantList { val } => Self::from_list(
                    val.into_iter()
                        .map(|c| return AssignmentValue::Single(Some(c)))
                        .collect_vec(),
                    tdef,
                    var_name,
                    scope_id,
                ),
                AE::Regex { rgx } => {
                    let list = list_from_rgx(&rgx, data);
                    log::debug!(
                        "{pre} Trying to assign value to list from regex matches [{matches}].",
                        pre = log_prefix_var_assignment(var_name, scope_id),
                        matches = list.iter().join(", ")
                    );
                    Self::from_list(list, tdef, var_name, scope_id)
                }
            };
        }

        return match acq {
            AE::Constant { val } => Self::from_single(Some(val), tdef, var_name, scope_id),
            AE::Regex { rgx } => {
                let val = rgx.captures_iter(data).next().and_then(|capture| {
                    let named_capture = capture.name(AE::REGEX_VAL_CAP_GROUP);
                    let default_capture = capture.get(0);

                    return named_capture
                        .or(default_capture)
                        .map(|mat| return mat.as_str().to_string());
                });

                if let Some(r#match) = val.as_ref() {
                    log::debug!(
                        "{pre} Trying to assign value from regex match '{match}'.",
                        pre = log_prefix_var_assignment(var_name, scope_id)
                    );
                } else {
                    log::warn!(
                        "{pre} No match found for regex '{rgx}'. Assigning NULL as fallback.",
                        pre = log_prefix_var_assignment(var_name, scope_id)
                    );
                };

                Self::from_single(val, tdef, var_name, scope_id)
            }
            AE::ConstantList { val } => Err(AssignmentHardError::InvalidValueShape {
                target_type: tdef.clone(),
                val: AssignmentValue::List(
                    val.into_iter()
                        .map(|c| return AssignmentValue::Single(Some(c)))
                        .collect_vec(),
                ),
                shape: AssignmentShape::Single,
                var_name: var_name.to_string(),
                scope_id,
            }),
        };
    }

    fn from_raw(
        raw: AssignmentValue,
        tdef: &VTypeDef,
        var_name: &str,
        scope_id: usize,
    ) -> Result<Self, AssignmentHardError> {
        return match raw {
            AssignmentValue::Single(str) => Self::from_single(str, tdef, var_name, scope_id),
            AssignmentValue::List(list) => Self::from_list(list, tdef, var_name, scope_id),
        };
    }

    fn from_single(
        str: Option<String>,
        tdef: &VTypeDef,
        var_name: &str,
        scope_id: usize,
    ) -> Result<Self, AssignmentHardError> {
        return match tdef {
            VTypeDef::String => Ok(Self::String { val: str }),
            VTypeDef::Int => Ok(Self::Int {
                val: str.and_then(|str| {
                    return str
                        .parse()
                        .inspect_err(|err| {
                            log_parse_value_as_type_failed(var_name, &str, tdef, err, scope_id);
                        })
                        .ok();
                }),
            }),
            VTypeDef::DateTime { fmts } => {
                let empty = Ok(Self::DateTime {
                    val: None,
                    fmt: None,
                });
                let Some(str) = str else { return empty };

                let Some((val, fmt)) = try_parse_until_valid_fmt(&str, fmts, |str, fmt| {
                    let datetime = NaiveDateTime::parse_from_str(str, fmt);
                    let date = NaiveDate::parse_from_str(str, fmt);
                    let time = NaiveTime::parse_from_str(str, fmt);

                    let fallback = match date {
                        Ok(date) => Some(NaiveDateTime::new(date, NaiveTime::MIN)),
                        Err(_) => match time {
                            Ok(time) => {
                                Some(NaiveDateTime::new(NaiveDateTime::UNIX_EPOCH.date(), time))
                            }
                            Err(_) => None,
                        },
                    };

                    let val = match datetime {
                        Ok(dt) => Ok(dt),
                        Err(err) => return fallback.ok_or(err),
                    };

                    return val.inspect_err(|err| {
                        log_parse_value_as_type_failed(var_name, str, tdef, err, scope_id);
                    });
                }) else {
                    return empty;
                };

                Ok(Self::DateTime {
                    val: Some(val),
                    fmt: Some(fmt),
                })
            }
            VTypeDef::Enum { variants } => Ok(Self::Enum {
                val: str.and_then(|str| {
                    return if variants.contains(&str) {
                        Some(str)
                    } else {
                        log_parse_value_as_type_failed(
                            var_name,
                            &str,
                            tdef,
                            AssignmentSoftError::InvalidEnumVariant {
                                valid_variants: variants.clone(),
                            },
                            scope_id,
                        );
                        None
                    };
                }),
            }),
            VTypeDef::List { .. } => Err(AssignmentHardError::InvalidValueShape {
                target_type: tdef.clone(),
                val: AssignmentValue::Single(str),
                shape: AssignmentShape::List,
                var_name: var_name.to_string(),
                scope_id,
            }),
        };
    }

    fn from_list(
        list: Vec<AssignmentValue>,
        tdef: &VTypeDef,
        var_name: &str,
        scope_id: usize,
    ) -> Result<Self, AssignmentHardError> {
        return match tdef {
            VTypeDef::List { item_type } => {
                assert!(
                    !matches!(&**item_type, VTypeDef::List { .. }),
                    "List cannot contain another nested list."
                );

                let mut vals = Vec::with_capacity(list.len());

                for raw in list {
                    let val = Self::from_raw(raw, item_type, var_name, scope_id)?;
                    if val.is_some() {
                        vals.push(val);
                    }
                }

                Ok(Self::List { val: vals })
            }
            _ => Err(AssignmentHardError::InvalidValueShape {
                val: AssignmentValue::List(list),
                target_type: tdef.clone(),
                shape: AssignmentShape::List,
                var_name: var_name.to_string(),
                scope_id,
            }),
        };
    }
}

impl VTypeDef {
    pub fn parse(
        name: &str,
        details: Option<VTypeAnnoDetails>,
        pos_tok_start: usize,
    ) -> Result<Self, VParseErr> {
        return match (name, details) {
            (Self::T_INT, None) => Ok(Self::Int),
            (Self::T_STRING, None) => Ok(Self::String),
            (Self::T_INT | Self::T_STRING, Some(dets)) => Err(VParseErr::TypeDoesntTakeDetails {
                r#type: name.to_string(),
                unneed_details: dets.to_string(),
                pos: pos_tok_start,
            }),
            (Self::T_DATE_TIME, Some(VTypeAnnoDetails::LiteralList(fmts))) => {
                Ok(Self::DateTime { fmts })
            }
            (Self::T_DATE_TIME, Some(VTypeAnnoDetails::Literal(fmt))) => {
                Ok(Self::DateTime { fmts: vec![fmt] })
            }
            (Self::T_ENUM, Some(VTypeAnnoDetails::LiteralList(variants))) => {
                let mut uniq = HashSet::new();
                let duplicate = variants.iter().find_map(|v| {
                    let already_used = !uniq.insert(v);
                    return already_used.then_some(v);
                });

                if let Some(duplicate_variant) = duplicate {
                    return Err(VParseErr::DuplicateEnumVariant {
                        variant: duplicate_variant.to_string(),
                        pos: pos_tok_start,
                    });
                }

                Ok(Self::Enum { variants })
            }
            (Self::T_ENUM, Some(VTypeAnnoDetails::Literal(variant))) => Ok(Self::Enum {
                variants: vec![variant],
            }),
            (Self::T_LIST, Some(VTypeAnnoDetails::NestedType(tdef))) => {
                if !Self::VALID_LIST_ITEM_TYPE_NAMES.contains(&tdef.name()) {
                    return Err(VParseErr::InvalidNestedType {
                        nested: tdef.name().to_string(),
                        parent: Self::T_LIST.to_string(),
                        valid: Self::VALID_LIST_ITEM_TYPE_NAMES,
                        pos: pos_tok_start,
                    });
                };
                Ok(Self::List {
                    item_type: Box::new(tdef),
                })
            }
            (Self::T_DATE_TIME | Self::T_ENUM | Self::T_LIST, Some(dets)) => {
                Err(VParseErr::InvalidTypeDetails {
                    received: dets,
                    pos: pos_tok_start,
                })
            }
            (Self::T_DATE_TIME | Self::T_ENUM | Self::T_LIST, None) => {
                Err(VParseErr::MissingTypeDetails {
                    r#type: name.to_string(),
                    required_details: Self::required_details(name).unwrap_or_default().to_string(),
                    pos: pos_tok_start,
                })
            }
            _ => Err(VParseErr::InvalidType {
                received: name.to_string(),
                valid: Self::VALID_TYPE_NAMES,
                pos: pos_tok_start,
            }),
        };
    }

    pub fn name(&self) -> &str {
        return match self {
            Self::String => Self::T_STRING,
            Self::Int => Self::T_INT,
            Self::DateTime { .. } => Self::T_DATE_TIME,
            Self::Enum { .. } => Self::T_ENUM,
            Self::List { .. } => Self::T_LIST,
        };
    }

    pub fn sql_type(&self) -> String {
        return match self {
            Self::Int | Self::DateTime { .. } => "INTEGER".to_string(),
            Self::String | Self::Enum { .. } => "TEXT".to_string(),
            Self::List { .. } => {
                panic!("list should never be checked for an sql type");
            }
        };
    }

    pub fn sql_meta_tbl(&self, field_name: &str) -> Option<String> {
        return match self {
            Self::Int | Self::String | Self::DateTime { .. } => None,
            Self::Enum { variants } => Some(gen_enum_meta_tbl(field_name, variants)),
            Self::List { item_type } => {
                assert!(
                    !matches!(&**item_type, VTypeDef::List { .. }),
                    "list should not contain a nested lists"
                );
                Some(gen_list_meta_tbl(field_name, item_type))
            }
        };
    }

    fn required_details(name: &str) -> Option<&str> {
        return match name {
            Self::T_DATE_TIME => Some("datetime format"),
            Self::T_ENUM => Some("list of variants separated by '|'"),
            Self::T_LIST => Some("type of list items"),
            _ => None,
        };
    }
}

fn list_from_rgx(rgx: &Regex, data: &str) -> Vec<AssignmentValue> {
    return rgx
        .captures_iter(data)
        .map(|capture| {
            let named_capture = capture.name(AE::REGEX_VAL_CAP_GROUP);
            let default_capture = capture.get(0);

            return AssignmentValue::Single(
                named_capture
                    .or(default_capture)
                    .map(|mat| return mat.as_str().to_string()),
            );
        })
        .collect();
}

impl std::fmt::Display for VTypeDef {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            Self::String => Self::T_STRING.to_string(),
            Self::Int => Self::T_INT.to_string(),
            Self::DateTime { fmts } => format!(
                "{t}<{fmts}>",
                t = Self::T_DATE_TIME,
                fmts = fmts.iter().map(|fmt| format!("'{fmt}'")).join("|")
            ),
            Self::Enum { variants } => {
                format!(
                    "{t}<{v}>",
                    t = Self::T_ENUM,
                    v = variants.iter().map(|v| format!("'{v}'")).join("|")
                )
            }
            Self::List { item_type } => {
                format!("{t}<{item_type}>", t = Self::T_LIST,)
            }
        };

        return write!(f, "{str}");
    }
}

impl std::fmt::Display for VTypeAnnoDetails {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            Self::Literal(literal) => format!("'{literal}'"),
            Self::LiteralList(list) => format!("[{list}]", list = list.join(",")),
            Self::NestedType(tdef) => tdef.to_string(),
        };

        return write!(f, "{str}");
    }
}

fn log_parse_value_as_type_failed<Err: Error>(
    var_name: &str,
    val: &str,
    r#type: &VTypeDef,
    err: Err,
    scope_id: usize,
) {
    log::warn!("{pre} Failed to assign value '{val}' to variable of type '{type}'. Assignment failed because '{err}'. Using NULL as a fallback.", pre = log_prefix_var_assignment(var_name, scope_id));
}

fn log_prefix_var_assignment(var_name: &str, scope_id: usize) -> String {
    return format!(
        "ASSIGMENT TO {var:10} CHUNK {scope_id}:",
        var = format!("'{var_name}';")
    );
}

fn try_parse_until_valid_fmt<T, Err, FnParse>(
    str: &str,
    fmts: &[String],
    parse: FnParse,
) -> Option<(T, String)>
where
    Err: Error,
    FnParse: Fn(&str, &str) -> Result<T, Err>,
{
    return fmts.iter().find_map(|fmt| {
        return parse(str, fmt)
            .map(|val| return (val, fmt.to_string()))
            .ok();
    });
}
