use itertools::Itertools;

use super::{parser::AcqExpr, types::VTypeDef};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct VTokenWithMeta {
    tok: VToken,
    pos: usize,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum VToken {
    VarDelimiter,
    AssignmentType,
    AssignmentAcqExpr,
    Name { val: String },
    TypeAnnotation { val: String },
    AcqExprType { val: char },
    ArrowL,
    ArrowR,
    Pipe,
    Comma,
    StringLiteral { val: String, closed: bool },
    Eof,
}

/// Useful for displaying in logs and errors messages. E.g. expected token of kind ...
#[derive(Debug)]
#[allow(unused)]
pub enum VTokenKind {
    VarDelimiter,
    AssignmentType,
    AssignmentAcqExpr,
    Name,
    TypeAnnotation,
    AcqExprType,
    ArrowL,
    ArrowR,
    Pipe,
    Comma,
    StringLiteral,
    Eof,
}

#[derive(Debug)]
#[allow(clippy::module_name_repetitions)]
pub struct VLexer {
    data: Vec<char>,
    pos: usize,
    read_head: usize,
    ch: Option<char>,
}

struct LiteralSpan<'a> {
    start: usize,
    chars: &'a [char],
    closed: bool,
}

impl VLexer {
    pub fn new(data: Vec<char>) -> Self {
        return Self {
            data,
            pos: 0,
            read_head: 0,
            ch: None,
        };
    }

    pub fn tokenize(&mut self) -> Vec<VTokenWithMeta> {
        let mut tokens: Vec<VTokenWithMeta> = vec![];
        loop {
            let token = self.next_token(tokens.last().map(|x| return x.tok()));

            if let VToken::Eof = token.tok() {
                break;
            }

            tokens.push(token);
        }

        return tokens;
    }

    fn next_token(&mut self, prev: Option<&VToken>) -> VTokenWithMeta {
        self.skip_whitespace();
        self.read_next_char();

        let Some(ch) = self.ch else {
            return VTokenWithMeta::new(VToken::Eof, self.pos);
        };

        match prev {
            Some(VToken::AssignmentAcqExpr) if ch.is_ascii_lowercase() => {
                return VTokenWithMeta::new(VToken::AcqExprType { val: ch }, self.pos);
            }
            Some(VToken::AssignmentType) => {
                let (start, ident) = self.read_ident();
                return VTokenWithMeta::new(
                    VToken::TypeAnnotation {
                        val: ident.iter().collect(),
                    },
                    start,
                );
            }
            Some(VToken::ArrowL) if ch != '\'' => {
                let (start, ident) = self.read_ident();
                return VTokenWithMeta::new(
                    VToken::TypeAnnotation {
                        val: ident.iter().collect(),
                    },
                    start,
                );
            }
            _ => {}
        };

        match ch {
            '=' => return VTokenWithMeta::new(VToken::AssignmentAcqExpr, self.pos),
            ':' => return VTokenWithMeta::new(VToken::AssignmentType, self.pos),
            ';' => return VTokenWithMeta::new(VToken::VarDelimiter, self.pos),
            '<' => return VTokenWithMeta::new(VToken::ArrowL, self.pos),
            '>' => return VTokenWithMeta::new(VToken::ArrowR, self.pos),
            ',' => return VTokenWithMeta::new(VToken::Comma, self.pos),
            '|' => return VTokenWithMeta::new(VToken::Pipe, self.pos),
            '\'' => {
                let LiteralSpan {
                    start,
                    chars,
                    closed,
                } = self.read_str_literal();

                return VTokenWithMeta::new(
                    VToken::StringLiteral {
                        val: chars.iter().collect(),
                        closed,
                    },
                    start,
                );
            }
            _ => {
                let (start, ident) = self.read_ident();
                return VTokenWithMeta::new(
                    VToken::Name {
                        val: ident.iter().collect(),
                    },
                    start,
                );
            }
        };
    }

    fn read_next_char(&mut self) {
        self.pos = self.read_head;
        self.read_head += 1;

        self.ch = if self.pos >= self.data.len() {
            None
        } else {
            Some(self.data[self.pos])
        };
    }

    fn skip_whitespace(&mut self) {
        let mut next = self.peak_next_char();
        while self.pos < self.data.len() && next.is_some_and(|c| return c.is_ascii_whitespace()) {
            self.read_next_char();
            next = self.peak_next_char();
        }
    }

    fn peak_next_char(&self) -> Option<char> {
        if self.read_head >= self.data.len() {
            return None;
        };

        return Some(self.data[self.read_head]);
    }

    fn read_ident(&mut self) -> (usize, &[char]) {
        let start = self.pos;
        while self.pos < self.data.len() && self.peak_next_char().is_some_and(is_ident_letter) {
            self.read_next_char();
        }

        return (start, &self.data[start..=self.pos]);
    }

    fn read_str_literal(&mut self) -> LiteralSpan<'_> {
        let start = self.read_head;

        let mut prev;
        let mut is_literal_closing = false;

        let closing = '\'';
        let escape = '\\';

        let mut closed = true;
        while !is_literal_closing {
            if self.pos >= self.data.len() {
                closed = false;
                break;
            }

            prev = self.ch;
            self.read_next_char();

            is_literal_closing = self.ch.is_some_and(|ch| return ch == closing)
                && prev.is_some_and(|ch| return ch != escape);
        }

        let start_with_tick = start - 1;
        return LiteralSpan {
            start: start_with_tick,
            chars: &self.data[start..self.pos],
            closed,
        };
    }
}

impl VTokenWithMeta {
    pub fn new(tok: VToken, pos: usize) -> Self {
        return Self { tok, pos };
    }

    pub fn tok(&self) -> &VToken {
        return &self.tok;
    }

    pub fn pos(&self) -> usize {
        return self.pos;
    }
}

impl std::fmt::Display for VToken {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            Self::Eof => "'EOF' (end of file)".to_string(),
            Self::VarDelimiter => "';' (variable delimiter)".to_string(),
            Self::AssignmentType => "':' (type assignment operator)".to_string(),
            Self::AssignmentAcqExpr => {
                "'=' (acquisition expression assignment operator)".to_string()
            }
            Self::ArrowL => "'<' (left arrow)".to_string(),
            Self::ArrowR => "'>' (right arrow)".to_string(),
            Self::Comma => "',' (comman)".to_string(),
            Self::Pipe => "'|' (Pipe)".to_string(),
            Self::AcqExprType { val } => format!("'{val}' (acquisition expression type name)"),
            Self::StringLiteral { val, .. } => format!("'{val}' (string literal)"),
            Self::TypeAnnotation { val } => format!("'{val}' (type name)"),
            Self::Name { val } => format!("'{val}' (variable name)"),
        };

        return write!(f, "{str}");
    }
}

impl std::fmt::Display for VTokenKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            Self::Eof => "'EOF'".to_string(),
            Self::VarDelimiter => "';'".to_string(),
            Self::AssignmentType => "':'".to_string(),
            Self::AssignmentAcqExpr => "'='".to_string(),
            Self::ArrowL => "'<'".to_string(),
            Self::ArrowR => "'>'".to_string(),
            Self::Comma => "','".to_string(),
            Self::Pipe => "'|'".to_string(),
            Self::AcqExprType => AcqExpr::VALID_ACQ_NAMES
                .iter()
                .map(|name| format!("'{name}' ({desc})", desc = AcqExpr::acq_description(*name)))
                .join(" | "),
            Self::StringLiteral => "'{some-literal}'".to_string(),
            Self::TypeAnnotation => VTypeDef::VALID_TYPE_NAMES
                .iter()
                .map(|name| format!("'{name}'"))
                .join("|"),
            Self::Name => "'{some-variable-name}'".to_string(),
        };

        return write!(f, "{str}");
    }
}

impl VToken {
    pub fn tok_len(&self) -> usize {
        return match self {
            Self::Eof
            | Self::Pipe
            | Self::Comma
            | Self::ArrowL
            | Self::ArrowR
            | Self::VarDelimiter
            | Self::AssignmentType
            | Self::AssignmentAcqExpr
            | Self::AcqExprType { .. } => 1,
            Self::Name { val } | Self::TypeAnnotation { val } => val.len(),
            Self::StringLiteral { val, closed } => {
                let count_quotes = if *closed { 2 } else { 1 };
                val.len() + count_quotes
            }
        };
    }
}

fn is_ident_letter(ch: char) -> bool {
    // a-z0-9_
    return ch.is_ascii_lowercase() || ch.is_ascii_digit() || ch == '_';
}

#[cfg(test)]
mod tests {
    use super::{VLexer, VToken, VTokenWithMeta};
    use itertools::Itertools;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_lexer_list_var() {
        let input: Vec<char> = "nums:list<int> = c'1','2','3'".chars().collect();
        let mut lexer = VLexer::new(input);
        let tokens = lexer
            .tokenize()
            .into_iter()
            .map(|x| return x.tok)
            .collect_vec();
        assert_eq!(
            tokens,
            vec![
                VToken::Name {
                    val: "nums".to_string()
                },
                VToken::AssignmentType,
                VToken::TypeAnnotation {
                    val: "list".to_string()
                },
                VToken::ArrowL,
                VToken::TypeAnnotation {
                    val: "int".to_string()
                },
                VToken::ArrowR,
                VToken::AssignmentAcqExpr,
                VToken::AcqExprType { val: 'c' },
                VToken::StringLiteral {
                    val: "1".to_string(),
                    closed: true
                },
                VToken::Comma,
                VToken::StringLiteral {
                    val: "2".to_string(),
                    closed: true
                },
                VToken::Comma,
                VToken::StringLiteral {
                    val: "3".to_string(),
                    closed: true
                },
            ]
        );
    }

    #[test]
    fn test_lexer_enum_var() {
        let input: Vec<char> = "state:enum<'todo'|'in-progress'|'done'>".chars().collect();
        let mut lexer = VLexer::new(input);
        let tokens = lexer
            .tokenize()
            .into_iter()
            .map(|x| return x.tok)
            .collect_vec();
        assert_eq!(
            tokens,
            vec![
                VToken::Name {
                    val: "state".to_owned()
                },
                VToken::AssignmentType,
                VToken::TypeAnnotation {
                    val: "enum".to_string()
                },
                VToken::ArrowL,
                VToken::StringLiteral {
                    val: "todo".to_string(),
                    closed: true
                },
                VToken::Pipe,
                VToken::StringLiteral {
                    val: "in-progress".to_string(),
                    closed: true
                },
                VToken::Pipe,
                VToken::StringLiteral {
                    val: "done".to_string(),
                    closed: true
                },
                VToken::ArrowR
            ]
        );
    }

    #[test]
    fn test_lexer_basic_var_with_pos() {
        let input: Vec<char> = "answer: int = c'42'".chars().collect();
        let mut lexer = VLexer::new(input);
        assert_eq!(
            lexer.tokenize(),
            vec![
                VTokenWithMeta::new(
                    VToken::Name {
                        val: "answer".to_string()
                    },
                    0
                ),
                VTokenWithMeta::new(VToken::AssignmentType, 6),
                VTokenWithMeta::new(
                    VToken::TypeAnnotation {
                        val: "int".to_string()
                    },
                    8
                ),
                VTokenWithMeta::new(VToken::AssignmentAcqExpr, 12),
                VTokenWithMeta::new(VToken::AcqExprType { val: 'c' }, 14),
                VTokenWithMeta::new(
                    VToken::StringLiteral {
                        val: "42".to_string(),
                        closed: true
                    },
                    15
                )
            ],
            "Basic single variable parsing should work"
        );
    }

    #[test]
    fn test_lexer_list_var_with_pos() {
        let input: Vec<char> = "answers:list<int>=c'42,42,42'".chars().collect();
        let mut lexer = VLexer::new(input);
        assert_eq!(
            lexer.tokenize(),
            vec![
                VTokenWithMeta::new(
                    VToken::Name {
                        val: "answers".to_string()
                    },
                    0
                ),
                VTokenWithMeta::new(VToken::AssignmentType, 7),
                VTokenWithMeta::new(
                    VToken::TypeAnnotation {
                        val: "list".to_string()
                    },
                    8
                ),
                VTokenWithMeta::new(VToken::ArrowL, 12),
                VTokenWithMeta::new(
                    VToken::TypeAnnotation {
                        val: "int".to_string()
                    },
                    13
                ),
                VTokenWithMeta::new(VToken::ArrowR, 16),
                VTokenWithMeta::new(VToken::AssignmentAcqExpr, 17),
                VTokenWithMeta::new(VToken::AcqExprType { val: 'c' }, 18),
                VTokenWithMeta::new(
                    VToken::StringLiteral {
                        val: "42,42,42".to_string(),
                        closed: true
                    },
                    19
                ),
            ],
            "Basic single variable parsing should work and support list types"
        );
    }

    #[test]
    fn test_lexer_multi_var_with_pos() {
        let input: Vec<char> = "answer:int=c'42'; empty:str=c'';find_the_answer: str = r'ANSWER: (?<val>\\'\\d\\d\\')'"
            .chars()
            .collect();
        let mut lexer = VLexer::new(input);
        assert_eq!(
            lexer.tokenize(),
            vec![
                VTokenWithMeta::new(
                    VToken::Name {
                        val: "answer".to_string()
                    },
                    0
                ),
                VTokenWithMeta::new(VToken::AssignmentType, 6),
                VTokenWithMeta::new(
                    VToken::TypeAnnotation {
                        val: "int".to_string()
                    },
                    7
                ),
                VTokenWithMeta::new(VToken::AssignmentAcqExpr, 10),
                VTokenWithMeta::new(VToken::AcqExprType { val: 'c' }, 11),
                VTokenWithMeta::new(
                    VToken::StringLiteral {
                        val: "42".to_string(),
                        closed: true
                    },
                    12
                ),
                VTokenWithMeta::new(VToken::VarDelimiter, 16),
                VTokenWithMeta::new(
                    VToken::Name {
                        val: "empty".to_string()
                    },
                    18
                ),
                VTokenWithMeta::new(VToken::AssignmentType, 23),
                VTokenWithMeta::new(
                    VToken::TypeAnnotation {
                        val: "str".to_string()
                    },
                    24
                ),
                VTokenWithMeta::new(VToken::AssignmentAcqExpr, 27),
                VTokenWithMeta::new(VToken::AcqExprType { val: 'c' }, 28),
                VTokenWithMeta::new(
                    VToken::StringLiteral {
                        val: String::new(),
                        closed: true
                    },
                    29
                ),
                VTokenWithMeta::new(VToken::VarDelimiter, 31),
                VTokenWithMeta::new(
                    VToken::Name {
                        val: "find_the_answer".to_string()
                    },
                    32
                ),
                VTokenWithMeta::new(VToken::AssignmentType, 47),
                VTokenWithMeta::new(
                    VToken::TypeAnnotation {
                        val: "str".to_string()
                    },
                    49
                ),
                VTokenWithMeta::new(VToken::AssignmentAcqExpr, 53),
                VTokenWithMeta::new(VToken::AcqExprType { val: 'r' }, 55),
                VTokenWithMeta::new(
                    VToken::StringLiteral {
                        val: "ANSWER: (?<val>\\'\\d\\d\\')".to_string(),
                        closed: true
                    },
                    56
                )
            ],
            "Multi variable parsing with escaped string literals should work"
        );
    }

    #[test]
    fn test_lexer_empty() {
        let input: Vec<char> = "".chars().collect();
        let mut lexer = VLexer::new(input);
        assert_eq!(
            lexer.tokenize().len(),
            0,
            "Empty variable string should return no tokens"
        );
    }
}
