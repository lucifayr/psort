use itertools::Itertools;
use regex::Regex;
use thiserror::Error;

use crate::var::lexer::VToken;

use super::{
    lexer::{VTokenKind, VTokenWithMeta},
    types::{VTypeAnnoDetails, VTypeDef},
};

#[derive(Debug)]
#[allow(clippy::module_name_repetitions)]
pub struct VParser {
    data: Vec<VTokenWithMeta>,
    pos: usize,
    read_head: usize,
    pos_next_tok_in_str: usize,
    tok: Option<VTokenWithMeta>,
}

#[derive(Debug)]
pub struct VDec {
    name: String,
    type_def: VTypeDef,
    acquisition_expression: AcqExpr,
}

/// {name}:{type}=<acq-expr>
///
/// <acq-expr> => {acq-expr-type}'{expr}'
/// e.g. c'42' = constant with value 42
/// e.g. r'.*:(?<val>.*)' = regex, value will be match of <val> capture group
#[derive(Debug, Clone)]
pub enum AcqExpr {
    Constant { val: String },
    ConstantList { val: Vec<String> },
    Regex { rgx: Regex },
}

#[derive(Debug, Error)]
pub enum VParseErr {
    #[error("Expected {valid} but received {re}",
            re = received.as_ref().map_or("<empty>".to_string(), |x| return x.to_string()),
            valid = expected.iter().map(|x| return x.to_string()).collect_vec().join(", "))]
    InvalidToken {
        pos: usize,
        received: Option<VToken>,
        expected: Vec<VTokenKind>,
    },
    #[error("Variable name `{name}` is used more than once. Make sure only 1 variable uses the name `{name}`.")]
    DuplicateVarName { name: String },
    #[error("Enum type definition contains duplicate variant '{variant}'. Make sure every enum variant is unique.")]
    DuplicateEnumVariant { variant: String, pos: usize },
    #[error("Invalid type `{received}` received, valid types are [{}].", valid.join(", "))]
    InvalidType {
        received: String,
        valid: &'static [&'static str],
        pos: usize,
    },
    #[error("Invalid nested type `{nested}` received for parent type `{parent}`, in this case valid nested types are [{}].", valid.join(", "))]
    InvalidNestedType {
        nested: String,
        parent: String,
        valid: &'static [&'static str],
        pos: usize,
    },
    #[error("Type `{r#type}` is missing required details `{required_details}`. Provided them like this `{r#type}<'...'>`.")]
    MissingTypeDetails {
        r#type: String,
        required_details: String,
        pos: usize,
    },
    #[error("Type `{r#type}` was passed with unnecessary details `{unneed_details}`. Remove the details and just pass the type as follows `{r#type}`.")]
    TypeDoesntTakeDetails {
        r#type: String,
        unneed_details: String,
        pos: usize,
    },
    #[error("Invalid type details `{received}` received.")]
    InvalidTypeDetails {
        received: VTypeAnnoDetails,
        pos: usize,
    },
    #[error("Invalid acquisition expression type `{received}` received, valid expressions are [{}].", valid.iter().map(|c| return c.to_string()).collect::<Vec<_>>().join(", "))]
    InvalidAcqType {
        received: char,
        valid: &'static [char],
        pos: usize,
    },
    #[error("{0}")]
    InvalidRegex(#[from] regex::Error),
    #[error("String literal `{literal}` is unclosed. Add the missing '.")]
    UnclosedStringLiteral { literal: String, pos: usize },
}

impl VParser {
    pub fn new(data: Vec<VTokenWithMeta>) -> Self {
        return Self {
            data,
            pos: 0,
            read_head: 0,
            pos_next_tok_in_str: 0,
            tok: None,
        };
    }

    pub fn parse(&mut self) -> Result<Vec<VDec>, Vec<VParseErr>> {
        let mut decs = vec![];
        let mut errs = vec![];

        loop {
            let res = self.next_var();
            match res {
                Ok(Some(dec)) => {
                    let duplicate_name = decs.iter().any(|x: &VDec| return x.name() == dec.name());

                    if duplicate_name {
                        errs.push(VParseErr::DuplicateVarName { name: dec.name });
                    } else {
                        decs.push(dec);
                    }
                }
                Ok(None) => break,
                Err(err) => {
                    self.read_to_next_var();
                    errs.push(err);
                }
            }
        }

        return if errs.is_empty() { Ok(decs) } else { Err(errs) };
    }

    fn next_var(&mut self) -> Result<Option<VDec>, VParseErr> {
        self.read_next_tok();
        let name = match &self.tok {
            Some(data) => Self::parse_var_name(data)?,
            None => return Ok(None),
        };

        self.read_and_valid_next_tok(
            |tok| matches!(tok, Some(VToken::AssignmentType)),
            vec![VTokenKind::AssignmentType],
        )?;

        let r#type = self.parse_type(None)?;

        self.read_and_valid_next_tok(
            |tok| matches!(tok, Some(VToken::AssignmentAcqExpr)),
            vec![VTokenKind::AssignmentAcqExpr],
        )?;

        let acq_expr = self.parse_acq_expr()?;

        self.read_and_valid_next_tok(
            |tok| matches!(tok, Some(VToken::VarDelimiter) | None),
            vec![VTokenKind::VarDelimiter],
        )?;

        return Ok(Some(VDec {
            name,
            type_def: r#type,
            acquisition_expression: acq_expr,
        }));
    }

    fn parse_type(&mut self, nested_name: Option<String>) -> Result<VTypeDef, VParseErr> {
        let type_name = if let Some(name) = nested_name {
            name
        } else {
            self.read_next_tok();
            let tok = self.tok.as_ref().map(|x| return x.tok());
            if let Some(VToken::TypeAnnotation { val }) = tok {
                val.clone()
            } else {
                return Err(VParseErr::InvalidToken {
                    pos: self.pos_next_tok_in_str,
                    received: tok.cloned(),
                    expected: vec![VTokenKind::TypeAnnotation],
                });
            }
        };

        let tok = self.peak_next_token();
        let type_details = if let Some(VToken::ArrowL) = tok.as_ref().map(|x| return x.tok()) {
            self.read_next_tok();

            let tok = self
                .peak_next_token()
                .as_ref()
                .map(|x| return x.tok())
                .cloned();
            match tok {
                Some(VToken::TypeAnnotation { val }) => {
                    self.read_next_tok();
                    let nested = self.parse_type(Some(val))?;

                    self.read_and_valid_next_tok(
                        |tok| matches!(tok, Some(VToken::ArrowR)),
                        vec![VTokenKind::ArrowR],
                    )?;

                    Some(VTypeAnnoDetails::NestedType(nested))
                }
                Some(VToken::StringLiteral { .. }) => {
                    let list = self.read_literal_list(|t| matches!(t, Some(VToken::Pipe)))?;

                    self.read_and_valid_next_tok(
                        |tok| matches!(tok, Some(VToken::ArrowR)),
                        vec![VTokenKind::ArrowR],
                    )?;

                    if list.len() == 1 {
                        Some(VTypeAnnoDetails::Literal(list[0].clone()))
                    } else {
                        Some(VTypeAnnoDetails::LiteralList(list))
                    }
                }
                _ => {
                    return Err(VParseErr::InvalidToken {
                        pos: self.pos_next_tok_in_str,
                        received: tok,
                        expected: vec![VTokenKind::StringLiteral, VTokenKind::TypeAnnotation],
                    })
                }
            }
        } else {
            None
        };

        return VTypeDef::parse(&type_name, type_details, self.pos_next_tok_in_str);
    }

    fn parse_acq_expr(&mut self) -> Result<AcqExpr, VParseErr> {
        self.read_next_tok();
        let tok = self.tok.as_ref().map(|x| return x.tok());
        let acq_type = if let Some(VToken::AcqExprType { val }) = tok {
            *val
        } else {
            return Err(VParseErr::InvalidToken {
                pos: self.pos_next_tok_in_str,
                received: tok.cloned(),
                expected: vec![VTokenKind::AcqExprType],
            });
        };

        let acq_detail_list = self.read_literal_list(|t| matches!(t, Some(VToken::Comma)))?;
        return AcqExpr::parse(acq_type, acq_detail_list, self.pos_next_tok_in_str);
    }

    fn parse_var_name(data: &VTokenWithMeta) -> Result<String, VParseErr> {
        let tok = data.tok();
        match tok {
            VToken::Name { val } => {
                return Ok(val.clone());
            }
            _ => {
                return Err(VParseErr::InvalidToken {
                    pos: data.pos(),
                    received: Some(tok.clone()),
                    expected: vec![VTokenKind::Name],
                })
            }
        }
    }

    fn read_literal_list<MFn: Fn(Option<VToken>) -> bool>(
        &mut self,
        matches: MFn,
    ) -> Result<Vec<String>, VParseErr> {
        let mut items = vec![self.read_str_literal()?];

        let mut next_tok = self
            .peak_next_token()
            .as_ref()
            .map(|x| return x.tok())
            .cloned();

        while matches(next_tok) {
            self.read_next_tok(); // pop comma
            let str = self.read_str_literal()?;
            items.push(str);

            next_tok = self
                .peak_next_token()
                .as_ref()
                .map(|x| return x.tok())
                .cloned();
        }

        return Ok(items);
    }

    fn read_str_literal(&mut self) -> Result<String, VParseErr> {
        self.read_next_tok();
        let tok = self.tok.as_ref().map(|x| return x.tok()).cloned();

        if let Some(VToken::StringLiteral { val, closed }) = tok {
            if !closed {
                return Err(VParseErr::UnclosedStringLiteral {
                    literal: val,
                    pos: self.pos_next_tok_in_str,
                });
            }

            return Ok(val);
        }

        return Err(VParseErr::InvalidToken {
            pos: self.pos_next_tok_in_str,
            received: tok,
            expected: vec![VTokenKind::StringLiteral],
        });
    }

    fn read_next_tok(&mut self) {
        self.pos_next_tok_in_str = self.next_tok_in_str();

        self.pos = self.read_head;
        self.read_head += 1;

        self.tok = if self.pos >= self.data.len() {
            None
        } else {
            Some(self.data[self.pos].clone())
        };
    }

    fn next_tok_in_str(&self) -> usize {
        let pos_next_tok_in_str = self.data.get(self.pos + 1).map(|t| return t.pos());
        if let Some(pos) = pos_next_tok_in_str {
            return pos;
        }

        let pos_after_str_end = self.data.last().map(|t| return t.pos() + t.tok().tok_len());
        if let Some(pos) = pos_after_str_end {
            return pos;
        }

        return 0;
    }

    fn read_and_valid_next_tok<MFn: Fn(Option<&VToken>) -> bool>(
        &mut self,
        matches: MFn,
        expected: Vec<VTokenKind>,
    ) -> Result<(), VParseErr> {
        self.read_next_tok();
        let tok = self.tok.as_ref().map(|x| return x.tok());
        if !matches(tok) {
            return Err(VParseErr::InvalidToken {
                pos: self.pos_next_tok_in_str,
                received: tok.cloned(),
                expected,
            });
        }

        return Ok(());
    }

    fn read_to_next_var(&mut self) {
        while let Some(tok) = self.tok.clone() {
            if *tok.tok() == VToken::VarDelimiter {
                break;
            }

            self.read_next_tok();
        }
    }

    fn peak_next_token(&self) -> Option<VTokenWithMeta> {
        if self.read_head >= self.data.len() {
            return None;
        }

        return Some(self.data[self.read_head].clone());
    }
}

impl VDec {
    #[cfg(test)]
    pub fn new(name: String, type_def: VTypeDef, acquisition_expression: AcqExpr) -> Self {
        return Self {
            name,
            type_def,
            acquisition_expression,
        };
    }

    pub fn name(&self) -> &str {
        return &self.name;
    }

    pub fn type_def(&self) -> &VTypeDef {
        return &self.type_def;
    }

    pub fn acquisition_expression(&self) -> &AcqExpr {
        return &self.acquisition_expression;
    }
}

impl AcqExpr {
    pub const VALID_ACQ_NAMES: &'static [char] = &['c', 'r'];
    pub const REGEX_VAL_CAP_GROUP: &'static str = "v";

    fn parse(name: char, details: Vec<String>, pos_tok_start: usize) -> Result<Self, VParseErr> {
        return match name {
            'c' if details.len() == 1 => Ok(Self::Constant {
                val: details[0].clone(),
            }),
            'c' if details.len() != 1 => Ok(Self::ConstantList { val: details }),
            'r' if details.len() == 1 => {
                let rgx = Regex::new(&details[0])?;
                Ok(Self::Regex { rgx })
            }
            'r' if details.len() != 1 => {
                Err(VParseErr::InvalidRegex(regex::Error::Syntax(format!(
                    "Can't use list [{list}] of for regex string",
                    list = details.join(",")
                ))))
            }
            _ => Err(VParseErr::InvalidAcqType {
                received: name,
                valid: Self::VALID_ACQ_NAMES,
                pos: pos_tok_start,
            }),
        };
    }

    pub fn acq_description<'a>(name: char) -> &'a str {
        debug_assert!(Self::VALID_ACQ_NAMES.contains(&name));

        return match name {
            'c' => "constant value",
            'r' => "regex",
            _ => "",
        };
    }
}

#[cfg(test)]
mod tests {

    use crate::var::{
        lexer::VLexer,
        parser::{AcqExpr, VParseErr, VParser, VTypeAnnoDetails, VTypeDef},
    };

    #[test]
    fn test_parser_basic_var() {
        let input: Vec<char> = "answer:int=c'42'".chars().collect();
        let mut lexer = VLexer::new(input);
        let tokens = lexer.tokenize();
        let mut parser = VParser::new(tokens);

        let out = parser.parse().unwrap();
        assert_eq!(out.len(), 1);
        assert_eq!(out[0].name, "answer");
        assert_eq!(out[0].type_def, VTypeDef::Int);
        assert!(matches!(
            out[0].acquisition_expression,
            AcqExpr::Constant { ref val } if val == "42"
        ));
    }

    #[test]
    fn test_parser_list() {
        let input: Vec<char> = "answers:list<int>=c'42','42','42'".chars().collect();
        let mut lexer = VLexer::new(input);
        let tokens = lexer.tokenize();
        let mut parser = VParser::new(tokens);
        let out = parser.parse().unwrap();
        assert_eq!(out.len(), 1);
        assert_eq!(out[0].name, "answers");
        assert_eq!(
            out[0].type_def,
            VTypeDef::List {
                item_type: Box::new(VTypeDef::Int)
            }
        );
        assert!(
            matches!(
                        out[0].acquisition_expression,
                        AcqExpr::ConstantList { ref val } if val == &["42".to_string(), "42".to_string(), "42".to_string()]
            ),
            "{out:?}"
        );

        let input: Vec<char> = "answers:list<int> = c'42'".chars().collect();
        let mut lexer = VLexer::new(input);
        let tokens = lexer.tokenize();
        let mut parser = VParser::new(tokens);
        let out = parser.parse().unwrap();
        assert_eq!(out.len(), 1);
        assert_eq!(out[0].name, "answers");
        assert_eq!(
            out[0].type_def,
            VTypeDef::List {
                item_type: Box::new(VTypeDef::Int)
            }
        );
        assert!(
            matches!(
                        out[0].acquisition_expression,
                        AcqExpr::Constant { ref val } if val == "42"
            ),
            "{out:?}"
        );

        let input: Vec<char> = "deep_nesting:list<list<datetime<'%T'>>>=c'template'"
            .chars()
            .collect();
        let mut lexer = VLexer::new(input);
        let tokens = lexer.tokenize();
        let mut parser = VParser::new(tokens);
        let out = parser.parse().unwrap_err();
        assert!(
            matches!(
                &out[..],
                [VParseErr::InvalidNestedType {
                    nested,
                    parent,
                    ..
                }, ..] if nested == VTypeDef::T_LIST && parent == VTypeDef::T_LIST
            ),
            "{out:?}"
        );
    }

    #[test]
    fn test_parser_datetime() {
        let input: Vec<char> = "answer:datetime<int>=c'invalid'".chars().collect();
        let mut lexer = VLexer::new(input);
        let tokens = lexer.tokenize();
        let mut parser = VParser::new(tokens);

        let out = parser.parse().unwrap_err();
        assert!(
            matches!(
                &out[..],
                [
                    VParseErr::InvalidTypeDetails {
                        received: VTypeAnnoDetails::NestedType(VTypeDef::Int),
                        ..
                    },
                    ..
                ]
            ),
            "{out:?}"
        );
    }

    #[test]
    fn test_parser_enum_var() {
        let input: Vec<char> = "state:enum<'done'> = c'done'".chars().collect();
        let mut lexer = VLexer::new(input);
        let tokens = lexer.tokenize();
        let mut parser = VParser::new(tokens);

        let out = parser.parse().unwrap();
        assert_eq!(out.len(), 1);
        assert_eq!(out[0].name, "state");
        assert_eq!(
            out[0].type_def,
            VTypeDef::Enum {
                variants: vec!["done".to_string()]
            }
        );
        assert!(matches!(
                    out[0].acquisition_expression,
                    AcqExpr::Constant { ref val } if val == "done",
        ));

        let input: Vec<char> = "state_invalid:enum<'done'|'todo'|'done'> = c'done'"
            .chars()
            .collect();
        let mut lexer = VLexer::new(input);
        let tokens = lexer.tokenize();
        let mut parser = VParser::new(tokens);

        let out = parser.parse().unwrap_err();
        assert!(
            matches!(
                &out[..],
                [VParseErr::DuplicateEnumVariant {  variant, .. }, ..] if variant == "done"
            ),
            "{out:?}"
        );

        let input: Vec<char> = "state:enum<'done'|'todo'> = c'done'".chars().collect();
        let mut lexer = VLexer::new(input);
        let tokens = lexer.tokenize();
        let mut parser = VParser::new(tokens);

        let out = parser.parse().unwrap();
        assert_eq!(out.len(), 1);
        assert_eq!(out[0].name, "state");
        assert_eq!(
            out[0].type_def,
            VTypeDef::Enum {
                variants: vec!["done".to_string(), "todo".to_string()]
            }
        );
        assert!(matches!(
                    out[0].acquisition_expression,
                    AcqExpr::Constant { ref val } if val == "done",
        ));
    }

    #[test]
    fn test_parser_multi_vars() {
        let input: Vec<char> =
            "due_date:datetime<'%Y-%m-%d'>=c'2024-12-31';find_the_answer: str = r'ANSWER: (?<val>\\'\\d\\d\\')';"
                .chars()
                .collect();
        let mut lexer = VLexer::new(input);
        let tokens = lexer.tokenize();
        let mut parser = VParser::new(tokens);

        let out = parser.parse().unwrap();
        assert_eq!(out.len(), 2);
        assert_eq!(&out[0].name, "due_date");
        assert_eq!(
            out[0].type_def,
            VTypeDef::DateTime {
                fmts: vec!["%Y-%m-%d".to_string()],
            }
        );
        assert!(matches!(
            &out[0].acquisition_expression,
            AcqExpr::Constant { val } if val == "2024-12-31"
        ));
        assert_eq!(&out[1].name, "find_the_answer");
        assert_eq!(out[1].type_def, VTypeDef::String);
        assert!(matches!(
            &out[1].acquisition_expression,
            AcqExpr::Regex { rgx } if rgx.as_str() == "ANSWER: (?<val>\\'\\d\\d\\')"
        ));
    }

    #[test]
    fn test_parser_invalid_literal() {
        let input: Vec<char> = "literal:str = c'hello world".chars().collect();
        let mut lexer = VLexer::new(input);
        let tokens = lexer.tokenize();
        let mut parser = VParser::new(tokens);
        let out = parser.parse().unwrap_err();

        assert!(
            matches!(
                &out[..],
                [VParseErr::UnclosedStringLiteral { literal, pos }] if literal == "hello world" && *pos == 15
            ),
            "{out:?}"
        );
    }

    #[test]
    fn test_parser_empty() {
        let input: Vec<char> = "".chars().collect();
        let mut lexer = VLexer::new(input);
        let tokens = lexer.tokenize();
        let mut parser = VParser::new(tokens);
        let out = parser.parse().unwrap();
        assert_eq!(out.len(), 0);
    }
}
