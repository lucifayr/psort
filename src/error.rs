use std::io;

use colored::Colorize;
use itertools::Itertools;
use thiserror::Error;

use crate::{
    sql::pre_process::SqlPreProcessErr,
    var::{parser::VParseErr, types::AssignmentHardError},
};

#[derive(Debug, Error)]
pub enum AppErr {
    #[error("VARIABLE PARSER ERRORS:\n{0}")]
    Parser(ParseErrs),
    #[error("IO ERROR:\n{0}")]
    Io(#[from] io::Error),
    #[error("SQL ERROR: {0}")]
    Sql(#[from] sqlite::Error),
    #[error("SQL PREPROCESSING ERROR: {0}")]
    SqlPreprocess(#[from] SqlPreProcessErr),
    #[error("DATA PROCESSING ERROR: variable assignment failures\n{errs}", errs = .0.iter().join("\n"))]
    VarProcessing(Vec<AssignmentHardError>),
}

#[derive(Debug)]
pub struct ParseErrs {
    errs: Vec<VParseErr>,
    var_str: String,
}

impl ParseErrs {
    pub fn new(errs: Vec<VParseErr>, var_str: String) -> Self {
        return Self { errs, var_str };
    }
}

impl std::fmt::Display for ParseErrs {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = self
            .errs
            .iter()
            .map(|err| {
                return fmt_parser_err(err, &self.var_str);
            })
            .rev()
            .join("\n\n\n");

        return write!(f, "{str}");
    }
}

fn fmt_parser_err(err: &VParseErr, var_str: &str) -> String {
    match err {
        VParseErr::InvalidToken { pos, received, .. } => {
            let tok_len = received.as_ref().map_or(1, |tok| return tok.tok_len());
            return highlight_err_in_str(var_str, &err.to_string(), *pos, tok_len);
        }
        VParseErr::UnclosedStringLiteral {
            literal: token,
            pos,
        }
        | VParseErr::InvalidType {
            received: token,
            pos,
            ..
        }
        | VParseErr::MissingTypeDetails {
            r#type: token, pos, ..
        } => return highlight_err_in_str(var_str, &err.to_string(), *pos, token.len()),
        VParseErr::InvalidTypeDetails { pos, .. } => {
            debug_assert!(
                *pos >= 1,
                "Type details should not be found at the start of the var string."
            );
            return highlight_err_in_str(var_str, &err.to_string(), *pos - 1, 1);
        }
        VParseErr::TypeDoesntTakeDetails {
            unneed_details,
            pos,
            ..
        } => {
            debug_assert!(
                *pos > unneed_details.len(),
                "Token position should be behind the type details."
            );
            return highlight_err_in_str(
                var_str,
                &err.to_string(),
                *pos - unneed_details.len(),
                unneed_details.len(),
            );
        }
        VParseErr::DuplicateEnumVariant { variant, pos } => {
            let tok_width = variant.len() + 2; // account for surrounding '
            debug_assert!(
                *pos > tok_width,
                "Token position should be behind enum variant."
            );
            return highlight_err_in_str(var_str, &err.to_string(), *pos - tok_width, tok_width);
        }
        VParseErr::InvalidAcqType { pos, .. } => {
            debug_assert!(
                *pos >= 1,
                "Acquisition expression should not be found at the start of the var string."
            );
            return highlight_err_in_str(var_str, &err.to_string(), *pos - 1, 1);
        }
        VParseErr::InvalidNestedType {
            nested,
            parent,
            pos,
            ..
        } => {
            // + 1 to account for the width of the '<'
            let type_chain_width = nested.len() + parent.len() + 1;
            debug_assert!(
                *pos > type_chain_width,
                "Starting position of nested type should be at the end of the type chain."
            );

            let nested_pos = *pos - type_chain_width;

            return highlight_err_in_str(var_str, &err.to_string(), nested_pos, nested.len());
        }
        _ => return format!("error:\n{err}"),
    };
}

fn highlight_err_in_str(var_str: &str, err_str: &str, pos_start: usize, width: usize) -> String {
    return format!(
        "error:\n{vstr}\n{p}{arrow}\n{p}{err}",
        vstr = var_str,
        p = " ".repeat(pos_start),
        arrow = "^".repeat(width).red(),
        err = err_str.red()
    );
}
