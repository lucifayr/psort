use itertools::Itertools;

use crate::{
    sql::schema::{
        AsSqlValue, DATE_COL, METATABLE_LIST_ID_FIELD_NAME, METATABLE_LIST_PREFIX,
        METATABLE_LIST_VALUE_FIELD_NAME, SCOPE_ID_COL,
    },
    var::{
        parser::VDec,
        types::{AssignmentHardError, VVal},
    },
};

use self::var_scope::ChunkVScope;

mod var_scope;
use crate::sql::schema::GLOBAL_TABLE_NAME;

#[derive(Debug)]
pub struct Chunk<'a> {
    id: usize,
    data: &'a str,
    vars: ChunkVScope,
}

type PreparedStatmentWithVals = (String, Vec<Option<String>>);
impl<'a> Chunk<'a> {
    pub fn new(id: usize, data: &'a str, decs: &[VDec]) -> Result<Self, AssignmentHardError> {
        let vars = ChunkVScope::new(data, decs, id)?;
        return Ok(Self { id, data, vars });
    }

    pub fn sql_insert_queries(&self) -> Vec<PreparedStatmentWithVals> {
        let mut names = vec![DATE_COL, SCOPE_ID_COL];
        let mut vals = vec![Some(self.data.to_string()), Some(self.id.as_sql_value())];

        let mut meta_tbl_inserts = vec![];

        for (name, val) in self.vars.scope() {
            if let VVal::List { val } = val {
                meta_tbl_inserts.push(sql_insert_list_meta_tbl(name, val, self.id));
            } else {
                names.push(name);
                vals.push(val.sql_data_str());
            }
        }

        return [(
            format!(
                "INSERT INTO {GLOBAL_TABLE_NAME} ({n}) VALUES ({v});",
                n = names.join(", "),
                v = (0..vals.len()).map(|_| return "?").join(",")
            ),
            vals,
        )]
        .into_iter()
        .chain(meta_tbl_inserts.into_iter().flatten())
        .collect();
    }
}

fn sql_insert_list_meta_tbl(
    name: &str,
    vals: &[VVal],
    scope_id: usize,
) -> Vec<PreparedStatmentWithVals> {
    return vals
        .iter()
        .map(|val| {
            return (
                format!("INSERT INTO {METATABLE_LIST_PREFIX}{name} ({METATABLE_LIST_ID_FIELD_NAME}, {METATABLE_LIST_VALUE_FIELD_NAME}) VALUES (?, ?);",),
                vec![Some(scope_id.as_sql_value()), val.sql_data_str()],
            );
        })
        .collect_vec();
}
