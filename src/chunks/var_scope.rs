use std::collections::HashMap;

use crate::var::parser::VDec;
use crate::var::types::{AssignmentHardError, VVal};

#[derive(Debug)]
pub struct ChunkVScope(HashMap<String, VVal>);

impl ChunkVScope {
    pub fn new(data: &str, decs: &[VDec], id: usize) -> Result<Self, AssignmentHardError> {
        let mut vars = HashMap::with_capacity(decs.len());

        for dec in decs {
            let acq_expr = dec.acquisition_expression();
            let name = dec.name();
            let val = VVal::from_acq(acq_expr.clone(), dec.type_def(), data, name, id)?;

            vars.insert(name.to_string(), val);
        }

        return Ok(Self(vars));
    }
    pub fn scope(&self) -> &HashMap<String, VVal> {
        return &self.0;
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use chrono::{NaiveDate, NaiveDateTime, NaiveTime};
    use regex::Regex;

    use crate::var::{
        parser::{AcqExpr, VDec},
        types::{VTypeDef, VVal},
    };

    use super::ChunkVScope;

    #[test]
    fn test_scope_creation() {
        let decs = vec![
            VDec::new(
                "answer".to_string(),
                VTypeDef::Int,
                AcqExpr::Constant {
                    val: "42".to_string(),
                },
            ),
            VDec::new(
                "due_date".to_string(),
                VTypeDef::DateTime {
                    fmts: vec!["%Y-%m-%d".to_string()],
                },
                AcqExpr::Regex {
                    rgx: Regex::new("due: (?<v>\\d{4}-\\d{2}-\\d{2})").unwrap(),
                },
            ),
            VDec::new(
                "niceness".to_string(),
                VTypeDef::Enum {
                    variants: vec!["NICE".to_owned(), "NOT-NICE".to_owned()],
                },
                AcqExpr::Regex {
                    rgx: Regex::new("ENUMS (?<v>\\w+)").unwrap(),
                },
            ),
        ];

        let chunk = r"
            hi,
            this is some test date with a due date (due: 2024-02-28)
            wow that is cool";

        let scope = ChunkVScope::new(chunk, &decs, 0).unwrap();
        assert_eq!(
            scope.0,
            HashMap::from([
                ("answer".to_string(), VVal::Int { val: Some(42) }),
                (
                    "due_date".to_string(),
                    VVal::DateTime {
                        val: Some(NaiveDateTime::new(
                            NaiveDate::from_ymd_opt(2024, 2, 28).unwrap(),
                            NaiveTime::MIN
                        )),
                        fmt: Some("%Y-%m-%d".to_string()),
                    }
                ),
                ("niceness".to_string(), VVal::Enum { val: None }),
            ])
        );

        let chunk = r"
            hello again,
            this is some test date without a due date :-:
            wow that is sad
            but we have ENUMS NICE";

        let scope = ChunkVScope::new(chunk, &decs, 0).unwrap();
        assert_eq!(
            scope.0,
            HashMap::from([
                ("answer".to_string(), VVal::Int { val: Some(42) }),
                (
                    "due_date".to_string(),
                    VVal::DateTime {
                        val: None,
                        fmt: None,
                    }
                ),
                (
                    "niceness".to_string(),
                    VVal::Enum {
                        val: Some("NICE".to_owned())
                    }
                ),
            ])
        );
    }

    #[test]
    fn test_scope_creation_with_list() {
        let decs = vec![VDec::new(
            "due_dates".to_string(),
            VTypeDef::List {
                item_type: Box::new(VTypeDef::DateTime {
                    fmts: vec!["%Y-%m-%d".to_string()],
                }),
            },
            AcqExpr::Regex {
                rgx: Regex::new("due: (?<v>\\d{4}-\\d{2}-\\d{2})").unwrap(),
            },
        )];
        let chunk = r"
            hello again,
            this is some test without multiple due dates /(*-*)/
            - due: 2024-02-28
            - due: 2023-03-27
            - due: 2025-01-29";

        let scope = ChunkVScope::new(chunk, &decs, 0).unwrap();
        assert_eq!(
            scope.0,
            HashMap::from([(
                "due_dates".to_string(),
                VVal::List {
                    val: vec![
                        VVal::DateTime {
                            val: Some(NaiveDateTime::new(
                                NaiveDate::from_ymd_opt(2024, 2, 28).unwrap(),
                                NaiveTime::MIN
                            )),
                            fmt: Some("%Y-%m-%d".to_string()),
                        },
                        VVal::DateTime {
                            val: Some(NaiveDateTime::new(
                                NaiveDate::from_ymd_opt(2023, 3, 27).unwrap(),
                                NaiveTime::MIN
                            )),
                            fmt: Some("%Y-%m-%d".to_string()),
                        },
                        VVal::DateTime {
                            val: Some(NaiveDateTime::new(
                                NaiveDate::from_ymd_opt(2025, 1, 29).unwrap(),
                                NaiveTime::MIN
                            )),
                            fmt: Some("%Y-%m-%d".to_string()),
                        }
                    ]
                }
            )])
        );
    }
}
