#![allow(clippy::needless_return)]
#![deny(clippy::implicit_return)]
#![deny(clippy::pedantic)]

use std::{
    fs,
    io::{self, Read},
    path::PathBuf,
    process::exit,
};

// TODO: add partial disabling of pre-processing on a per type basis

use chunks::Chunk;
use clap::Parser;
use env_logger::Env;
use error::{AppErr, ParseErrs};
use itertools::Itertools;
use rayon::prelude::{IndexedParallelIterator, IntoParallelRefIterator, ParallelIterator};
use regex::Regex;
use sql::schema::gen_create_tbl_queries;
use sqlite::Connection;
use var::{parser::VDec, types::AssignmentHardError};

use crate::{
    sql::pre_process::map_query_vars,
    var::{lexer::VLexer, parser::VParser},
};

mod chunks;
mod error;
mod sql;
mod var;

#[derive(Debug, Parser)]
#[clap(author, version)]
#[allow(clippy::struct_excessive_bools)]
struct CliArgs {
    /// Variable definitions
    #[arg(short, long, default_value_t = String::from(""))]
    variables: String,

    /// SQL expression
    #[arg(short, long)]
    expression: String,

    /// Don't apply pre-processing to SQL expression. Pre-processing allows using types like lists
    /// that are non native to SQL.
    #[arg(short = 'P', long)]
    no_preprocessing: bool,

    input_file: Option<PathBuf>,

    /// Use matches from this regex instead of splitting the input string by delimiter.
    #[arg(short = 'R', long)]
    regex: Option<Regex>,

    /// Enables verbose logging. Equivalent to setting ``PQL_LOG`` to ``pql=warn``
    /// like this ``PQL_LOG=pql=warn``
    #[arg(long)]
    verbose: bool,

    /// Keep string split matches in output data.
    #[arg(short = 'K', long, conflicts_with = "regex")]
    keep_separator: bool,

    /// Pattern that is used to split chunks in input.
    #[arg(short = 's', long, default_value_t = String::from("\n\n"))]
    in_separator: String,
    /// String that is used to separate chunks in output.
    #[arg(short = 'S', long)]
    out_separator: Option<String>,
    /// String that  is used to separate fields in a chunk in output.
    #[arg(short = 'F', long, default_value_t = String::from("\n"))]
    out_field_separator: String,
}

struct Separatos {
    input: String,
    output: String,
    output_field: String,
}

fn main() {
    let args = CliArgs::parse();

    if args.verbose {
        std::env::set_var("PQL_LOG", "pql=info");
    };
    env_logger::Builder::from_env(Env::default().filter_or("PQL_LOG", "pql=error")).init();

    let int_sep = insert_escape_codes(&args.in_separator);
    let out_sep = insert_escape_codes(args.out_separator.as_ref().unwrap_or(&args.in_separator));
    let out_field_sep = insert_escape_codes(&args.out_field_separator);

    let seps = Separatos {
        input: int_sep,
        output: out_sep,
        output_field: out_field_sep,
    };

    let res = run(&args, &seps);
    if let Err(err) = res {
        eprintln!("{err}");
        exit(1);
    }
}

fn run(args: &CliArgs, seps: &Separatos) -> Result<(), AppErr> {
    let var_decs = parse_vars(&args.variables)?;
    let data = read_input_data(args.input_file.as_ref()).unwrap();

    let con = sqlite::open(":memory:")?;
    let tbl_schema = gen_create_tbl_queries(&var_decs);
    con.execute(tbl_schema)?;

    let chunks = load_chunks(
        data.trim(),
        &seps.input,
        &var_decs,
        args.regex.clone(),
        args.keep_separator,
    );

    let chunks = no_errors_in_chunks(chunks)?;

    sql_insert_data(&con, chunks)?;
    let rows = sql_query_data(
        &con,
        &args.expression,
        args.no_preprocessing,
        &var_decs,
        &seps.output_field,
    )?;

    let out = rows.join(&seps.output);
    println!("{out}");

    return Ok(());
}

fn parse_vars(variables: &str) -> Result<Vec<VDec>, AppErr> {
    let decs = if variables.is_empty() {
        vec![]
    } else {
        let mut lexer = VLexer::new(variables.chars().collect());
        VParser::new(lexer.tokenize())
            .parse()
            .map_err(|errs| return AppErr::Parser(ParseErrs::new(errs, variables.to_string())))?
    };

    return Ok(decs);
}

fn read_input_data(path: Option<&PathBuf>) -> Result<String, io::Error> {
    let Some(path) = path else {
        return Ok(read_input_data_stdin());
    };

    let data = fs::read_to_string(path)?;
    return Ok(data);
}

fn read_input_data_stdin() -> String {
    let mut stdin = std::io::stdin().lock();
    let mut data = String::new();

    while let Ok(n_bytes) = stdin.read_to_string(&mut data) {
        if n_bytes == 0 {
            break;
        }
    }

    return data;
}

fn load_chunks<'a>(
    data: &'a str,
    sep_str: &str,
    decs: &[VDec],
    rgx: Option<Regex>,
    keep_sep: bool,
) -> Vec<Result<Chunk<'a>, AssignmentHardError>> {
    let data_vec = match (rgx, keep_sep) {
        (Some(rgx), _) => split_rgx(&rgx, data),
        (None, true) => split_keep(sep_str, data),
        (None, false) => data.split(sep_str).collect_vec(),
    };

    return data_vec
        .par_iter()
        .enumerate()
        .map(|(idx, chunk)| return Chunk::new(idx, chunk.trim(), decs))
        .collect();
}

fn no_errors_in_chunks(
    chunks: Vec<Result<Chunk<'_>, AssignmentHardError>>,
) -> Result<Vec<Chunk<'_>>, AppErr> {
    let has_errs = chunks.iter().any(Result::is_err);

    if has_errs {
        let errs = chunks
            .into_iter()
            .filter_map(|x| return if let Err(err) = x { Some(err) } else { None })
            .collect_vec();

        return Err(AppErr::VarProcessing(errs));
    }

    return Ok(chunks.into_iter().flatten().collect_vec());
}

fn sql_query_data(
    con: &Connection,
    query: &str,
    no_preprocessing: bool,
    var_decs: &[VDec],
    field_sep: &str,
) -> Result<Vec<String>, AppErr> {
    let query = if no_preprocessing {
        query.to_owned()
    } else {
        map_query_vars(query, var_decs)?
    };

    let mut out = vec![];
    con.iterate(query, |pairs| {
        let mut fields = vec![];
        for (_name, val) in pairs {
            fields.push(
                val.map(|str| return str.to_string())
                    .unwrap_or("NULL".to_string()),
            );
        }

        out.push(fields.join(field_sep));
        return true;
    })?;

    return Ok(out);
}

fn sql_insert_data(con: &sqlite::Connection, chunks: Vec<Chunk<'_>>) -> Result<(), sqlite::Error> {
    con.execute("PRAGMA journal_mode = OFF; PRAGMA synchronous = 0; PRAGMA cache_size = 1000000; PRAGMA locking_mode = EXCLUSIVE;")?;

    for chunk in chunks {
        for (query, vals) in chunk.sql_insert_queries() {
            let mut pre = con.prepare(query)?;
            for (idx, val) in vals.into_iter().enumerate() {
                pre.bind((idx + 1, val.as_deref()))?;
            }

            for res in pre {
                res?;
            }
        }
    }

    return Ok(());
}

fn split_keep<'a>(pat: &str, data: &'a str) -> Vec<&'a str> {
    let mut result = Vec::new();
    let mut last = 0;
    for (index, matched) in data.match_indices(pat) {
        let end = index + matched.len();
        result.push(&data[last..end]);
        last = end;
    }

    if last < data.len() {
        result.push(&data[last..]);
    }

    return result;
}

fn split_rgx<'a>(rgx: &Regex, data: &'a str) -> Vec<&'a str> {
    let use_named_group = rgx.capture_names().any(|name| return name == Some("v"));

    return rgx
        .captures_iter(data)
        .flat_map(|caps| {
            return if use_named_group {
                [caps.name("v").map(|mat| return mat.as_str())]
                    .into_iter()
                    .flatten()
                    .collect_vec()
            } else {
                caps.iter()
                    .filter_map(|mat| return mat.map(|str| return str.as_str()))
                    .collect_vec()
            };
        })
        .collect_vec();
}

fn insert_escape_codes(str: &str) -> String {
    return str
        .replace("\\n", "\n")
        .replace("\\r", "\r")
        .replace("\\t", "\t");
}
