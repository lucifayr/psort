use itertools::Itertools;
use sqlparser::{
    ast::{
        Expr, FunctionArg, FunctionArgExpr, FunctionArguments, Ident, OrderByExpr, SelectItem,
        SetExpr, Statement, Value,
    },
    dialect::SQLiteDialect,
    parser::{Parser, ParserError},
};
use thiserror::Error;

use crate::var::{parser::VDec, types::VTypeDef};

use super::utils::{
    sql_coalesce_chain_fn, sql_enum_ord_sub_select, sql_fn, sql_list_aggregation_query,
    sql_list_val_concat, sql_list_vals_query, sql_strftime_from_unix_millis_fn,
};

#[derive(Debug, Error)]
pub enum SqlPreProcessErr {
    #[error("{0}")]
    Parse(#[from] ParserError),
    #[error(
        "Invalid SQL statment provided. Only queries (SELECTs) are allowed when pre-processing is enabled.\nSTATEMENT: '{stmt}'"
    )]
    InvalidStatement { stmt: Statement },
    #[error(
        "Invalid SQL query type. Only SELECT queries are allowed when pre-processing is enabled.\nQUERY: '{query}'"
    )]
    InvalidQueryType { query: SetExpr },
}

/// Process SQL statement to add support for non-native SQL features such as lists.
/// Transforms specific queries like checking if a value is in a list `... IN (my_list)`
/// into valid SQL `... IN (SELECT value FROM {list_meta_table} WHERE ...)`
pub fn map_query_vars(query: &str, var_decs: &[VDec]) -> Result<String, SqlPreProcessErr> {
    let ast = Parser::parse_sql(&SQLiteDialect {}, query)?;

    let mut processed = Vec::with_capacity(ast.len());
    for stmt in ast {
        let processed_stm = map_stmt_vars(stmt, var_decs)?;
        processed.push(processed_stm);
    }

    let query = processed.join("; ");
    return Ok(query);
}

fn map_stmt_vars(stmt: Statement, var_decs: &[VDec]) -> Result<String, SqlPreProcessErr> {
    return match stmt {
        Statement::Query(mut query) => {
            *query.body = map_stmt_select_body_vars(*query.body, var_decs)?;
            query.order_by = map_stmt_order_by_vars(query.order_by, var_decs);
            Ok(query.to_string())
        }
        _ => Err(SqlPreProcessErr::InvalidStatement { stmt }),
    };
}

fn map_stmt_select_body_vars(
    body: SetExpr,
    var_decs: &[VDec],
) -> Result<SetExpr, SqlPreProcessErr> {
    let SetExpr::Select(mut select) = body else {
        return Err(SqlPreProcessErr::InvalidQueryType { query: body });
    };

    select.projection = map_select_projection_vars(select.projection, var_decs);
    select.selection = select
        .selection
        .map(|expr| return map_stmt_where_clause(expr, var_decs));

    return Ok(SetExpr::Select(select));
}

fn map_select_projection_vars(projection: Vec<SelectItem>, var_decs: &[VDec]) -> Vec<SelectItem> {
    let mapped_items = projection
        .into_iter()
        .map(|item| return map_select_item(item, var_decs))
        .collect();

    return mapped_items;
}

fn map_select_item(item: SelectItem, var_decs: &[VDec]) -> SelectItem {
    let mapped_item = match item {
        SelectItem::UnnamedExpr(expr) => {
            SelectItem::UnnamedExpr(map_select_item_expr(expr, var_decs))
        }
        SelectItem::ExprWithAlias { expr, alias } => SelectItem::ExprWithAlias {
            expr: map_select_item_expr(expr, var_decs),
            alias,
        },
        _ => item,
    };

    return mapped_item;
}

fn map_select_item_expr(expr: Expr, var_decs: &[VDec]) -> Expr {
    let ident = match expr {
        Expr::Identifier(ident) => ident,
        Expr::Function(_) => {
            return map_list_aggregation_func(expr.clone(), var_decs);
        }
        _ => {
            return expr;
        }
    };

    let name = &ident.value;
    let type_def = var_decs.iter().find_map(|d| {
        return (d.name() == name).then_some(d.type_def());
    });

    let mapped_ident_expr = match type_def {
        Some(VTypeDef::Int | VTypeDef::String | VTypeDef::Enum { .. }) | None => {
            Expr::Identifier(ident.clone())
        }
        Some(VTypeDef::DateTime { fmts }) => {
            // not great
            // will often lead to unnecessary format mismatches
            let fmt_funcs = fmts
                .iter()
                .map(|fmt| {
                    return sql_strftime_from_unix_millis_fn(name, fmt);
                })
                .collect_vec();

            sql_coalesce_chain_fn(fmt_funcs)
        }
        Some(VTypeDef::List { .. }) => Expr::Subquery(Box::new(sql_list_val_concat(name))),
    };

    return mapped_ident_expr;
}

fn map_stmt_where_clause(expr: Expr, var_decs: &[VDec]) -> Expr {
    return match expr {
        Expr::InList {
            expr,
            list,
            negated,
        } => map_in_list_conditional(expr, list, negated, var_decs),
        Expr::Function(_) => map_list_aggregation_func(expr, var_decs),
        Expr::BinaryOp { left, op, right } => Expr::BinaryOp {
            left: Box::new(map_stmt_where_clause(*left, var_decs)),
            right: Box::new(map_stmt_where_clause(*right, var_decs)),
            op,
        },
        _ => expr,
    };
}

fn map_in_list_conditional(
    expr: Box<Expr>,
    list: Vec<Expr>,
    negated: bool,
    var_decs: &[VDec],
) -> Expr {
    if let [Expr::Identifier(Ident { value, .. })] = &list[..] {
        let name = value;
        let type_def = var_decs.iter().find_map(|d| {
            return (d.name() == name).then_some(d.type_def());
        });

        if let Some(VTypeDef::List { .. }) = type_def {
            return Expr::InSubquery {
                subquery: Box::new(sql_list_vals_query(name)),
                expr,
                negated,
            };
        }
    };

    return Expr::InList {
        list,
        expr,
        negated,
    };
}

fn map_stmt_order_by_vars(order_by: Vec<OrderByExpr>, var_decs: &[VDec]) -> Vec<OrderByExpr> {
    let mapped_vars = order_by
        .into_iter()
        .map(|mut ord_expr| {
            ord_expr.expr = match ord_expr.expr {
                Expr::Identifier(ident) => map_order_by_ident(ident, var_decs),
                Expr::Function(_) => map_list_aggregation_func(ord_expr.expr, var_decs),
                _ => ord_expr.expr,
            };

            return ord_expr;
        })
        .collect();

    return mapped_vars;
}

fn map_order_by_ident(ident: Ident, var_decs: &[VDec]) -> Expr {
    let name = &ident.value;
    let type_def = var_decs
        .iter()
        .find_map(|d| return (d.name() == name).then_some(d.type_def()));

    let mapped_expr = match type_def {
        Some(VTypeDef::Int | VTypeDef::String | VTypeDef::DateTime { .. }) | None => {
            Expr::Identifier(ident)
        }
        Some(VTypeDef::Enum { .. }) => Expr::Subquery(Box::new(sql_enum_ord_sub_select(name))),
        Some(VTypeDef::List { .. }) => Expr::Value(Value::Null),
    };

    return mapped_expr;
}

fn map_list_aggregation_func(expr: Expr, var_decs: &[VDec]) -> Expr {
    let Expr::Function(ref func) = expr else {
        return expr;
    };

    let fn_name = &func.name.to_string();
    let FunctionArguments::List(ref list) = func.args else {
        return expr;
    };

    let [FunctionArg::Unnamed(arg_expr)] = &list.args[..] else {
        return expr;
    };

    match arg_expr {
        FunctionArgExpr::Expr(Expr::Identifier(Ident { value, .. })) => {
            let var_name = value;
            let type_def = var_decs.iter().find_map(|dec| {
                return (dec.name() == var_name).then_some(dec.type_def());
            });

            if let Some(VTypeDef::List { .. }) = type_def {
                return Expr::Subquery(Box::new(sql_list_aggregation_query(var_name, fn_name)));
            }
        }
        FunctionArgExpr::Expr(expr) if matches!(expr, Expr::Function(_)) => {
            return Expr::Function(sql_fn(
                fn_name,
                vec![FunctionArg::Unnamed(FunctionArgExpr::Expr(
                    map_list_aggregation_func(expr.clone(), var_decs),
                ))],
            ))
        }
        _ => {}
    }

    return expr;
}

#[cfg(test)]
mod tests {
    use crate::var::{
        parser::{AcqExpr, VDec},
        types::VTypeDef,
    };

    use super::map_query_vars;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_pre_process_select() {
        let decs = [
            VDec::new(
                "integer".to_string(),
                VTypeDef::Int,
                AcqExpr::Constant {
                    val: "placeholder".to_string(),
                },
            ),
            VDec::new(
                "date".to_string(),
                VTypeDef::DateTime {
                    fmts: vec!["%Y-%m-%d".to_string(), "%d/%m/%Y".to_string()],
                },
                AcqExpr::Constant {
                    val: "placeholder".to_string(),
                },
            ),
            VDec::new(
                "enum".to_string(),
                VTypeDef::Enum {
                    variants: vec!["var 1".to_string(), "var 2".to_string()],
                },
                AcqExpr::Constant {
                    val: "placeholder".to_string(),
                },
            ),
            VDec::new(
                "list".to_string(),
                VTypeDef::List {
                    item_type: Box::new(VTypeDef::Int),
                },
                AcqExpr::Constant {
                    val: "placeholder".to_string(),
                },
            ),
        ];

        let stmt = "SELECT integer, date FROM g WHERE integer > 128";
        let mapped = map_query_vars(stmt, &decs).unwrap();
        assert_eq!(
            mapped,
            "SELECT integer, COALESCE(STRFTIME('%Y-%m-%d', date / 1000, 'unixepoch'), STRFTIME('%d/%m/%Y', date / 1000, 'unixepoch')) FROM g WHERE integer > 128"
        );

        let stmt = "SELECT enum FROM g WHERE integer IN (list)";
        let mapped = map_query_vars(stmt, &decs).unwrap();
        assert_eq!(
            mapped,
            "SELECT enum FROM g WHERE integer IN (SELECT value FROM meta_list_list WHERE id = scope_id)"
        );

        let stmt = "SELECT list FROM g ORDER BY enum";
        let mapped = map_query_vars(stmt, &decs).unwrap();
        assert_eq!(
            mapped,
            "SELECT (SELECT GROUP_CONCAT(value) FROM meta_list_list WHERE id = scope_id) FROM g ORDER BY (SELECT COALESCE((SELECT ordering FROM meta_enum_enum WHERE value = enum), 9223372036854775807))"
        );
    }
}
