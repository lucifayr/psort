use super::schema::{
    METATABLE_ENUM_ORD_FIELD_NAME, METATABLE_ENUM_PREFIX, METATABLE_ENUM_VALUE_FIELD_NAME,
    METATABLE_LIST_ID_FIELD_NAME, METATABLE_LIST_PREFIX, METATABLE_LIST_VALUE_FIELD_NAME,
    SCOPE_ID_COL,
};
use sqlparser::{
    ast::{
        BinaryOperator, Expr, Function, FunctionArg, FunctionArgExpr, FunctionArgumentList,
        FunctionArguments, Ident, ObjectName, Query, Value,
    },
    dialect::SQLiteDialect,
    parser::Parser,
};

pub fn sql_coalesce_chain_fn(exprs: Vec<Expr>) -> Expr {
    assert!(!exprs.is_empty());

    if exprs.len() == 1 {
        return exprs[0].clone();
    }

    return Expr::Function(sql_fn(
        "COALESCE",
        exprs
            .into_iter()
            .map(|expr| {
                return FunctionArg::Unnamed(FunctionArgExpr::Expr(expr));
            })
            .collect(),
    ));
}

pub fn sql_strftime_from_unix_millis_fn(var_name: &str, chrono_fmt: &str) -> Expr {
    return Expr::Function(sql_fn(
        "STRFTIME",
        vec![
            FunctionArg::Unnamed(FunctionArgExpr::Expr(Expr::Value(
                Value::SingleQuotedString(fmt_chrono_to_sql_lossy(chrono_fmt)),
            ))),
            FunctionArg::Unnamed(FunctionArgExpr::Expr(Expr::BinaryOp {
                left: Box::new(Expr::Identifier(Ident::new(var_name))),
                op: BinaryOperator::Divide,
                right: Box::new(Expr::Value(Value::Number(1000.to_string(), false))),
            })),
            FunctionArg::Unnamed(FunctionArgExpr::Expr(Expr::Value(
                Value::SingleQuotedString("unixepoch".to_string()),
            ))),
        ],
    ));
}

pub fn sql_fn(name: &str, args: Vec<FunctionArg>) -> Function {
    return Function {
        name: ObjectName(vec![Ident::new(name)]),
        args: FunctionArguments::List(FunctionArgumentList {
            duplicate_treatment: None,
            args,
            clauses: vec![],
        }),
        filter: None,
        null_treatment: None,
        over: None,
        within_group: vec![],
    };
}

pub fn sql_enum_ord_sub_select(name: &str) -> Query {
    let int_max = i64::MAX;
    let inner_query = format!("SELECT {METATABLE_ENUM_ORD_FIELD_NAME} FROM {METATABLE_ENUM_PREFIX}{name} WHERE {METATABLE_ENUM_VALUE_FIELD_NAME} = {name}");
    let query = format!("SELECT COALESCE(({inner_query}), {int_max})");
    let body = Box::new(
        Parser::new(&SQLiteDialect {})
            .try_with_sql(&query)
            .unwrap()
            .parse_query_body(0)
            .unwrap(),
    );

    return Query {
        body,
        with: None,
        order_by: vec![],
        limit: None,
        limit_by: vec![],
        offset: None,
        fetch: None,
        locks: vec![],
        for_clause: None,
    };
}

pub fn sql_list_vals_query(name: &str) -> Query {
    let query = format!("SELECT {METATABLE_LIST_VALUE_FIELD_NAME} FROM {METATABLE_LIST_PREFIX}{name} WHERE {METATABLE_LIST_ID_FIELD_NAME} = {SCOPE_ID_COL}");
    let body = Box::new(
        Parser::new(&SQLiteDialect {})
            .try_with_sql(&query)
            .unwrap()
            .parse_query_body(0)
            .unwrap(),
    );

    return Query {
        body,
        with: None,
        order_by: vec![],
        limit: None,
        limit_by: vec![],
        offset: None,
        fetch: None,
        locks: vec![],
        for_clause: None,
    };
}

pub fn sql_list_aggregation_query(name: &str, fn_name: &str) -> Query {
    let query = format!("SELECT {fn_name}({METATABLE_LIST_VALUE_FIELD_NAME}) FROM {METATABLE_LIST_PREFIX}{name} WHERE {METATABLE_LIST_ID_FIELD_NAME} = {SCOPE_ID_COL}");
    let body = Box::new(
        Parser::new(&SQLiteDialect {})
            .try_with_sql(&query)
            .unwrap()
            .parse_query_body(0)
            .unwrap(),
    );

    return Query {
        body,
        with: None,
        order_by: vec![],
        limit: None,
        limit_by: vec![],
        offset: None,
        fetch: None,
        locks: vec![],
        for_clause: None,
    };
}

pub fn sql_list_val_concat(name: &str) -> Query {
    let query = format!("SELECT GROUP_CONCAT({METATABLE_LIST_VALUE_FIELD_NAME}) FROM {METATABLE_LIST_PREFIX}{name} WHERE {METATABLE_LIST_ID_FIELD_NAME} = {SCOPE_ID_COL}");
    let body = Box::new(
        Parser::new(&SQLiteDialect {})
            .try_with_sql(&query)
            .unwrap()
            .parse_query_body(0)
            .unwrap(),
    );

    return Query {
        body,
        with: None,
        order_by: vec![],
        limit: None,
        limit_by: vec![],
        offset: None,
        fetch: None,
        locks: vec![],
        for_clause: None,
    };
}

/// See chrono docs: <https://docs.rs/chrono/latest/chrono/format/strftime/index.html/>
/// See sqlite docs: <https://www.sqlitetutorial.net/sqlite-date-functions/sqlite-strftime-function/>
/// Ignores chrono fraction specifiers (e.g. %9f)
pub fn fmt_chrono_to_sql_lossy(fmt: &str) -> String {
    return fmt
        .replace("%C", "%Y")
        .replace("%y", "%Y")
        .replace("%h", "") // -> month name x
        .replace("%b", "") // -> month name x
        .replace("%B", "") // -> month name x
        .replace("%f", "") // nanoseconds x
        .replace("%Z", "") // timezone x
        .replace("%z", "") // timezone x
        .replace("%:z", "") // timezone x
        .replace("%::z", "") // timezone x
        .replace("%:::z", "") // timezone x
        .replace("%#z", "") // timezone x
        .replace("%a", "%w") // Sun -> 0, Tue -> 2
        .replace("%A", "%w") // Sunday -> 0, Tuesday -> 2
        .replace("%u", "%w") // Sunday: 7 -> 0
        .replace("%U", "%W")
        .replace("%V", "%W")
        .replace("%G", "%Y")
        .replace("%g", "%Y")
        .replace("%D", "%m/%d/%Y")
        .replace("%x", "%m/%d/%Y")
        .replace("%F", "%Y-%m-%d")
        .replace("%v", "%e-%m-%Y")
        .replace("%R", "%H:%M")
        .replace("%T", "%H:%M:%S")
        .replace("%X", "%H:%M:%S")
        .replace("%r", "%I:%M:%S %P")
        .replace("%c", "%d/%m/%Y %H:%M:%S")
        .replace("%+", "%Y-%m-%dT%H:%M:%f+00:00")
        .replace("%t", "\t")
        .replace("%n", "\n");
}
