use chrono::{NaiveDate, NaiveDateTime, NaiveTime};
use itertools::Itertools;

use crate::var::{parser::VDec, types::VTypeDef};

pub const GLOBAL_TABLE_NAME: &str = "g";
pub const DATE_COL: &str = "data";
pub const SCOPE_ID_COL: &str = "scope_id";

pub const METATABLE_ENUM_PREFIX: &str = "meta_enum_";
pub const METATABLE_ENUM_ORD_FIELD_NAME: &str = "ordering";
pub const METATABLE_ENUM_VALUE_FIELD_NAME: &str = "value";

pub const METATABLE_LIST_PREFIX: &str = "meta_list_";
pub const METATABLE_LIST_ID_FIELD_NAME: &str = "id";
pub const METATABLE_LIST_VALUE_FIELD_NAME: &str = "value";

pub trait AsSqlValue {
    fn as_sql_value(&self) -> String;
}

impl AsSqlValue for i64 {
    fn as_sql_value(&self) -> String {
        return self.to_string();
    }
}

impl AsSqlValue for usize {
    fn as_sql_value(&self) -> String {
        return self.to_string();
    }
}

impl AsSqlValue for String {
    fn as_sql_value(&self) -> String {
        return self.to_string();
    }
}

impl AsSqlValue for NaiveDate {
    fn as_sql_value(&self) -> String {
        return self
            .signed_duration_since(NaiveDateTime::UNIX_EPOCH.into())
            .num_milliseconds()
            .as_sql_value();
    }
}

impl AsSqlValue for NaiveTime {
    fn as_sql_value(&self) -> String {
        return self
            .signed_duration_since(NaiveTime::MIN)
            .num_milliseconds()
            .as_sql_value();
    }
}

impl AsSqlValue for NaiveDateTime {
    fn as_sql_value(&self) -> String {
        return self
            .signed_duration_since(NaiveDateTime::UNIX_EPOCH)
            .num_milliseconds()
            .as_sql_value();
    }
}

pub fn gen_create_tbl_queries(var_decs: &[VDec]) -> String {
    let mut queries = vec![gen_global_tbl(var_decs)];

    for dec in var_decs {
        if let Some(meta_tbl_query) = dec.type_def().sql_meta_tbl(dec.name()) {
            queries.push(meta_tbl_query);
        }
    }

    return queries.join(";");
}

pub fn gen_enum_meta_tbl(tbl_name: &str, variants: &[String]) -> String {
    let value_field = format!(
        "{METATABLE_ENUM_VALUE_FIELD_NAME} TEXT CHECK ({METATABLE_ENUM_VALUE_FIELD_NAME} in ({valid}))",
        valid = variants.iter().map(|v| return format!("'{v}'")).join(",")
    );
    let mut queries = vec![format!(
        "CREATE TABLE {METATABLE_ENUM_PREFIX}{tbl_name} ({value_field}, {METATABLE_ENUM_ORD_FIELD_NAME} INTEGER)"
    )];

    let mut inserts = variants
        .iter()
        .enumerate()
        .map(|(idx, variant)| {
            return format!("INSERT INTO {METATABLE_ENUM_PREFIX}{tbl_name} ({METATABLE_ENUM_VALUE_FIELD_NAME}, {METATABLE_ENUM_ORD_FIELD_NAME}) VALUES ('{variant}', {idx})");
        })
        .collect_vec();

    queries.append(&mut inserts);
    return queries.join(";");
}

pub fn gen_list_meta_tbl(tbl_name: &str, nested_type: &VTypeDef) -> String {
    return format!(
        "CREATE TABLE {METATABLE_LIST_PREFIX}{tbl_name} ({METATABLE_LIST_ID_FIELD_NAME} INTEGER, {METATABLE_LIST_VALUE_FIELD_NAME} {t})",
        t = nested_type.sql_type()
    );
}

fn gen_global_tbl(var_decs: &[VDec]) -> String {
    let mut cols = vec![
        format!("{DATE_COL} TEXT"),
        format!("{SCOPE_ID_COL} INTEGER"),
    ];
    let mut var_cols = var_decs
        .iter()
        .filter(|dec| return !matches!(dec.type_def(), VTypeDef::List { .. }))
        .map(|dec| {
            return format!("{n} {t}", n = dec.name(), t = dec.type_def().sql_type());
        })
        .collect_vec();

    cols.append(&mut var_cols);
    return format!(
        "CREATE TABLE {GLOBAL_TABLE_NAME} ({col_defs});",
        col_defs = cols.join(", ")
    );
}
