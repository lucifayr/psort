!!! outdated

# order-by parsing specification

> Only used to drive development at the moment not a guaranteed language spec.

## Variable declaration

### Format

```
-> `{name}:{type}={acquisition-expression}`

e.g.
-> `answer:int=c'42'`
    ^       ^  ^  ^
   variable |  |  |
    name    |  | expression value (can be a regex for dynamic values)
            |  |
     |-> type  |
     |        expression type (constant)
     |
    for non-string types a conversion is performed,
    the value is set to None if the conversion fails
```

### Types

1. `int`: Type
2. `date<'%Y-%m-%d'>`: Type with extra details (e.g. date format)
3. `list<int>`: Type with other nested type

### Example

```sh
order-by -v "title:str=r'# (?<val>.*)$'; due-date:date<'%Y-%m-%d'>=r':url: (?<val>http[s]://.*)';
max:uint=c'65535'; list-items:list<str>=r'^- (?<val>.*)$'"
```

- Define the **variable** `title` of type `str` (string). The value of `title` will be the
  first value matched by the named capture group `val` in the regex (`r'...'`) `#
(?<val>.*)$` in a chunk. If no value is found or the `val` group doesn't exist `title`
  is set to `None`.
- Define the **variable** `due-date` of type `date`. The same matching rules as for
  `title` apply to `due-date`, but if a match is found the value must also be convertible
  to `date` of the format `%Y-%m-%d` otherwise the value is set to `None`.
- Define a **constant** `max` of type `uint` (unsigned int), with the value 65535.
- Define a **variable** `list-items` of the type `list<str>`. The vale of `list-items` is
  equal to all values matched by the named capture group `val` in the regex (`r'...'`) `#
(?<val>.*)$` in a chunk. If no value is found or the `val` group doesn't exist
  `list-items` is set to an empty list.

### Type Conversion

```sh
... "... order by due-date as date<'%Y-%m-%d'>" # try converting a value to a date and us the order logic provided for dates
```

## Expression Parsing

### Keywords

- `select`
  > default: `select data` = display full chunk data
- `where`
  - `or`
  - `and`
  - `==`, `!=`, `>`, `<`, `<=`, `>=`
- `as` parse into other type
- `order by`
- `asc` direction ascending
- `desc` direction descending

#### Reserved variables for select

- `idx`: index of the output chunks after being filtered.
- `date`: chunk data, full plain text

#### Examples

```sh
order-by -e "select data where due-date > '2024-01-01' as date<'%Y-%m-%d'> order by due-date asc"

order-by -e "select word-count where word-count < '500' as uint and idx < '50' as uint order by word-count desc"
```
