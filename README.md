# PQL 

Wouldn't it be cool if you could use SQL on unstructured text
documents... Now you can!

```sh
cat numbers.txt

  # Output:
  #   1 | 2 | 3
  #   4 | 5 | 6 | 3
  #   1 | 3 | 5
  #   2 | 4

```


```sh

cat numbers.txt \
  | pql --variables "nums:list<int> = r'\d+'" \
    --expression "SELECT AVG(nums) FROM g" \
    --in-separator "\n"

  # Output:
  #   2.0
  #   4.5
  #   3.0
  #   3.0
```


## How it works

In the `--variables | -v` field you provide a list of variables to `PQL`. These
variables are then used to create a matching SQLite table (named `g`). Then the
input string/file is read and split into chunks based on the `--in-separator`
field. Now the table (`g`) is populated with 1 value per variable per chunk
(Variable values are parsed from the chunks).

A variable is of the following shape: `{name}:{type} = {acquisition-method}`, e.g. `myvar:string = r'hello (.*)'`

The variable name can contain any of the following characters: `[A-Za-Z0-9]` and `-`.

The variable type determines how a value is parsed from a chunk and can be any of the following:

- `int`: A 64 bit signed integer
- `string`: A variable length string
- `datetime<'fmt1' | 'fmt2' ...>`: A date time matching one of the provide formats
- `enum<'variant1' | 'variant2' ...>`: A string that matches one of the provided variants
- `list<T>`: A list of elements of type `T`

The "acquisition-method" determines how the value for a variable is found. Usually this will be a regex `r'{some regex}'`.

> By default the entire regex match will be parsed into the variables value.
>
> You can specify a specific capture group to be parsed instead of the entire match by naming the capture group `v`, e.g. `r'title: (?<v>.*)'`.
